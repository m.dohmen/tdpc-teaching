# TdPC-Teaching

This git repository contains a jupyter notebook which you can download and open in the gwdg jupyter cloud. For that purpose visit https://jupyter-cloud.gwdg.de/, upload the file to be able to view and change the notebook.

Additionally, xyz files are provided if you want to follow along the steps taken in this script with your own codes.
